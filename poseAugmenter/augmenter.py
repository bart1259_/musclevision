from numpy import SHIFT_INVALID
import glob
import random
import re
import os
import random
import math
import warnings
import pandas as pd
from pandas.core.common import SettingWithCopyWarning

warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

SOURCE_DIR="./poseNetwork/Input"
OUTPUT_DIT="./poseAugmenter/output"
SHIFT_AMOUNT = 0.05
AUGMENTATION_ROTATIONS_PER_VIDEO = 5

def augment():
    input_files = glob.glob(f"{SOURCE_DIR}/**/*.csv", recursive=True)
    for input_file in input_files:
        for i in range(AUGMENTATION_ROTATIONS_PER_VIDEO):
            for flip in range(2):
                folder_name = re.search(r'(\w*)\\\w*\.csv$', input_file).group(1)
                file_name = re.search(r'(\w*\.csv)$', input_file).group(1)
                df_data = pd.read_csv(input_file, index_col="frame")

                # Flip if needed
                if flip % 2 == 0:
                    x_data = df_data.filter(regex=(r'\w+\.x'))
                    x_data = x_data.apply(lambda a: a * -1)
                    df_data[x_data.columns] = x_data[x_data.columns]

                # Apply Rotation
                rads = math.radians(random.uniform(0.0, 360.0))

                xs = df_data[df_data.columns[::4]]
                zs = df_data[df_data.columns[2::4]]

                for col in xs.columns:
                    zcol = col.replace("x","z")
                    xs[col] = (xs[col] * math.cos(rads)) - (zs[zcol] * math.sin(rads))
                    zs[zcol] = (zs[zcol] * math.cos(rads)) + (xs[col] * math.sin(rads))

                df_data[df_data.columns[::4]] = xs
                df_data[df_data.columns[2::4]] = zs

                new_data = df_data.apply(lambda a: a + ((2 * (random.random() - 0.5)) * SHIFT_AMOUNT))
                output_folder = f"{OUTPUT_DIT}/{folder_name}"
                output_file = f"{output_folder}/{file_name}_{i}_{'flipped' if flip % 2 == 0 else ''}.csv"
                print(output_file)
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)
                new_data.to_csv(output_file)

augment()